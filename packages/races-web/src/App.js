import React from "react";
import { Root } from "races-shared";

export default function App() {
  return <Root />;
}
