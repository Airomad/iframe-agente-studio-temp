const fs = require("fs");
const path = require("path");
const promisify = require("util.promisify");

const { Client } = require("fb-watchman");
const rimraf = require("rimraf");

const statAsync = promisify(fs.stat);
const linkAsync = promisify(fs.link);
const utimesAsync = promisify(fs.utimes);
const mkdirAsync = promisify(fs.mkdir);
const unlinkAsync = promisify(fs.unlink);
const rmdirRecursive = promisify(rimraf);

module.exports = async function watchLinks(src, dest, verbose) {
  try {
    const client = new Client();

    const capabilityCheckAsync = promisify(client.capabilityCheck.bind(client));
    const commandAsync = promisify(client.command.bind(client));

    await capabilityCheckAsync({ optional: [], required: ["relative_root"] });

    const srcPath = path.resolve(process.cwd(), src);
    const destPath = path.resolve(process.cwd(), dest);

    await mkdirWithParents(srcPath, destPath, true, verbose);

    const { warning, watch, relative_path } = await commandAsync([
      "watch-project",
      srcPath
    ]);

    if (warning) console.warn(warning);

    console.log(
      `Watch established on ${watch}, relative path ${relative_path}`
    );

    const subscriptionName = ["watchLinks", srcPath, destPath].join("__");
    const subscriptionOptions = {
      fields: ["name", "exists", "type", "atime", "mtime"],
      relative_root: relative_path
    };

    const { subscribe } = await commandAsync([
      "subscribe",
      watch,
      subscriptionName,
      subscriptionOptions
    ]);

    console.log(`Subscription ${subscribe} established`);

    client.on("subscription", ({ subscription, files }) => {
      if (subscription !== subscriptionName) return;

      files.forEach(({ name, exists, type, atime, mtime }) => {
        const srcFilePath = path.join(srcPath, name);
        const destFilePath = path.join(destPath, name);

        fs.access(destFilePath, async err => {
          try {
            const destMissing = !!err;

            if (exists) {
              if (destMissing) {
                await mkdirWithParents(
                  srcFilePath,
                  destFilePath,
                  type === "d",
                  verbose
                );

                if (type === "f") {
                  await linkAsync(srcFilePath, destFilePath);
                  if (verbose)
                    console.log(`Link ${srcFilePath} => ${destFilePath}`);
                }
              } else {
                await utimesAsync(destFilePath, +atime, +mtime);
                if (verbose)
                  console.log(`Touch ${srcFilePath} => ${destFilePath}`);
              }
            } else if (!(exists || destMissing)) {
              if (type === "f") {
                await unlinkAsync(destFilePath);
                if (verbose) console.log("Unlink", destFilePath);
              } else if (type === "d") {
                await rmdirRecursive(destFilePath);
                if (verbose) console.log("Remove dir", destFilePath);
              }
            }
          } catch (err) {
            console.error(err);
          }
        });
      });
    });
  } catch (err) {
    console.error(err);

    client.end();
  }
};

async function mkdirWithParents(
  srcFilePath,
  destFilePath,
  isDir = false,
  verbose = false
) {
  const parentSrcPath = path.dirname(srcFilePath);
  const parentDestPath = path.dirname(destFilePath);

  try {
    if (parentDestPath !== "/") {
      await mkdirWithParents(parentSrcPath, parentDestPath, true);
    }

    if (isDir) {
      const srcBasename = path.basename(srcFilePath);
      const destBasename = path.basename(srcFilePath);

      const { mode } = srcBasename === destBasename
        ? await statAsync(srcFilePath)
        : {};

      await mkdirAsync(destFilePath, mode);
      if (verbose) console.log("Make dir", destFilePath);
    }
  } catch (err) {
    if (err.code !== "EEXIST") throw err;
  }
}
