const fs = require("fs");
const path = require("path");
const promisify = require("util.promisify");

const { dependencies } = require("../package.json");

const watchLinks = require("./watch-links");

const readdirAsync = promisify(fs.readdir);
const unlinkAsync = promisify(fs.unlink);

async function syncInternalDeps(verbose = false) {
  try {
    const packages = await readdirAsync(path.resolve(__dirname, "..", ".."));

    const internalDeps = packages.filter(pkg => dependencies.hasOwnProperty(pkg));

    try {
      await Promise.all(
        internalDeps.map(async name => {
          const depPath = path.resolve(__dirname, "..", "node_modules", name);

          await unlinkAsync(depPath);
          if (verbose) console.log("Unlink", depPath);
        })
      );
    } catch (err) {
      if (err.code !== "EPERM") throw err;
    }

    return Promise.all(
      internalDeps.map(name =>
        watchLinks(
          path.resolve(__dirname, "..", "..", name),
          path.resolve(__dirname, "..", "node_modules", name),
          verbose
        )
      )
    );
  } catch (err) {
    console.error(err);
  }
}

const [verbose] = process.argv.slice(2)

syncInternalDeps(!!verbose);
