import React from "react";
import { Root } from "races-shared";

export default class App extends React.Component {
  render() {
    return <Root />;
  }
}
